export const footNavContents = [
  {
    title: 'Privacy',
    slug: '/privacypolicy',
  },
  {
    title: 'Contribute',
    slug: '/contribute',
  },
  {
    title: 'DMCA',
    slug: 'https://tally.so/r/nPdGW5',
  },
  {
    title: 'Discord',
    slug: 'https://discord.gg/AMrV3yjC4y',
  },
  {
    title: 'Matrix',
    slug: 'https://matrix.to/#/#morrismotel:morrismotel.com',
  },
];
